package com.arians.auctusbackend.model

data class TransactionDTO (

    val id: String = "",
    val timestamp: String = ""

)
package com.arians.auctusbackend.model

import java.math.BigDecimal

data class BurnDTO (
     val id: String,
     val acoName: String,
     val collateralAmount: BigDecimal,
     val tokenAmount: BigDecimal,
     val timestamp: String
)
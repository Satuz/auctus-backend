package com.arians.auctusbackend.model


data class TransactionInfo (val monthly : List<Pair<String,Int>> , val overallCount : Int, val lastMonthCount : Int, val lastWeekCount : Int, val lastDayCount : Int, val lastUpdated: String)

enum class MONTHLY_TIMESTAMP(val timestamp: Int){


    JUNE_20(1590962400),
    JULY_20(1593554400),
    AUGUST_20(1596232800),
    SEPTEMBER_20(1598911200),
    OCTOBER_20(1601503200),
    NOVEMBER_20(1604185200),
    DECEMBER_20(1606777200),
    JANUARY_21(1609459200),
    FEBRUARY_21(1612137600),
    MARCH_21(1614556800),
    APRIL_21(1617235200),
    MAY_21(1619827200),
    JUNE_21(1622505600),
    JULY_21(1625097600),
    AUGUST_21(1627776000),
    SEPTEMBER_21(1630454400),
    OCTOBER_21(1633046400),
    NOVEMBER_21(1635724800),
    DECEMBER_21(1638316800)
}

data class TimelineNumbers(val overallCount: Double, val lastMonthCount: Double, val lastWeekCount: Double, val lastDayCount:Double)

data class BurnInfo(val burnings : TimelineNumbers, val tokenAmount : TimelineNumbers, val collateralAmount : TimelineNumbers, val lastUpdated: String)
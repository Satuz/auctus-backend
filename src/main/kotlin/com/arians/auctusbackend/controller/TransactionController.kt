package com.arians.auctusbackend.controller


import com.arians.auctusbackend.model.TransactionDTO
import com.arians.auctusbackend.model.TransactionInfo
import com.arians.auctusbackend.service.BurnService
import com.arians.auctusbackend.service.TransactionService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TransactionController(val transactionService: TransactionService, val burnService: BurnService) {

    @GetMapping("/transactions")
    fun allTransactions(): ResponseEntity<List<TransactionDTO>> {
        return ResponseEntity.ok(transactionService.getTransactions())
    }

    @GetMapping("/transactionInfo")
    fun getTransactionInfo(): ResponseEntity<TransactionInfo> {
        return ResponseEntity.ok(transactionService.getTransactionInfos())
    }

    @PostMapping("/fetch")
    fun fetchTransactions(): ResponseEntity<Void> {
        transactionService.fetchTransactions()
        burnService.fetchBurns()
        return ResponseEntity.ok().build()
    }
}
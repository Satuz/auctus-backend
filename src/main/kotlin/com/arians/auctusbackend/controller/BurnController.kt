package com.arians.auctusbackend.controller

import com.arians.auctusbackend.model.BurnInfo
import com.arians.auctusbackend.service.BurnService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/burnings")
class BurnController(val burnService: BurnService) {

    @GetMapping("/info")
    fun getBurnInfo(): ResponseEntity<BurnInfo> {
      return  ResponseEntity.ok(burnService.getBurnInfo())
    }
}
package com.arians.auctusbackend.repository

import com.arians.auctusbackend.model.TransactionDTO
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@Repository
class TransactionRepository(val jdbcTemplate: JdbcTemplate) {

    fun batchInsert(transactions: List<TransactionDTO>, batchSize: Int): Array<IntArray?> {
        return jdbcTemplate.batchUpdate(
            "insert into transaction (id, timestamp) values(?,?)",
            transactions,
            batchSize
        ) { ps, argument ->
            ps.setString(1, argument.id)
            ps.setString(2, argument.timestamp)
        }
    }

    fun getLatestTransactionTimestamp(): Int? {
        return jdbcTemplate.queryForObject("select max(timestamp) from transaction", Int::class.java)
    }

    fun getLatestTransaction(): TransactionDTO? {
        return jdbcTemplate.queryForObject("select id ,timestamp from transaction where timestamp = (select max(timestamp) from transaction)", TransactionDTO::class.java)
    }

    fun getTransactions(): List<TransactionDTO> {
        return jdbcTemplate.queryForList("Select * from transaction").map { TransactionDTO(it["id"] as String, it["timestamp"] as String) }
    }

    fun setLastUpdated() {
        val now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
        jdbcTemplate.update("insert into update_history (id, last_updated) VALUES (?, ?) ", UUID.randomUUID(), now)
    }

    fun getLastUpdate() : String{
        return jdbcTemplate.queryForObject("select max(last_updated) from update_history",String::class.java) ?: "no update"
    }
}
package com.arians.auctusbackend.repository

import com.arians.auctusbackend.model.BurnDTO
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import java.time.format.DateTimeFormatter
import java.util.*

@Repository
class BurnRepository(val jdbcTemplate: JdbcTemplate) {

    fun batchInsert(burns: List<BurnDTO>, batchSize: Int): Array<IntArray?> {
        return jdbcTemplate.batchUpdate(
            "insert into burn (id, timestamp, aco_name, collateralAmount, tokenAmount) values(?,?,?,?,?)",
            burns,
            batchSize
        ) { ps, argument ->
            ps.setString(1, argument.id)
            ps.setString(2, argument.timestamp)
            ps.setString(3, argument.acoName)
            ps.setBigDecimal(4, argument.collateralAmount)
            ps.setBigDecimal(5, argument.tokenAmount)
        }
    }

    fun getLatestBurnTimestamp(): Int {
        return jdbcTemplate.queryForObject("select max(timestamp) from burn", Int::class.java) ?: 0
    }




   fun getBurnings(): List<BurnDTO> {
        return jdbcTemplate.queryForList("Select * from burn").map {
            BurnDTO(id= it["id"] as String,
                acoName = it["acoName"] as String,
                timestamp = it["timestamp"] as String,
                tokenAmount = it["tokenAmount"] as BigDecimal,
                collateralAmount = it["collateralAmount"] as BigDecimal)
        }

    }

}
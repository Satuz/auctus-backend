package com.arians.auctusbackend.service

import com.arians.auctusbackend.generated.AllTransactions
import com.arians.auctusbackend.generated.alltransactions.Transaction
import com.arians.auctusbackend.model.MONTHLY_TIMESTAMP
import com.arians.auctusbackend.model.TransactionDTO
import com.arians.auctusbackend.model.TransactionInfo
import com.arians.auctusbackend.repository.TransactionRepository
import com.expediagroup.graphql.client.spring.GraphQLWebClient
import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.Instant


@Service
class TransactionService(val transactionRepository: TransactionRepository) {

    companion object {
        private val LOGGER: Logger = LoggerFactory.getLogger(TransactionService::class.java)
    }

    @Value("\${theGraph.url}")
    val theGraphUrl: String = ""

    fun getTransactions(): List<TransactionDTO> {
        return transactionRepository.getTransactions()
    }

    fun getTransactionInfos(): TransactionInfo {
        val transactions = getTransactions()
        //server timezone
        val now = Instant.now().epochSecond
        val lastDayCount = transactions.filter { it.timestamp.toInt() > (now - 86400) }.size
        val lastWeekCount = transactions.filter { it.timestamp.toInt() > (now - 604800) }.size
        val lastMonthCount = transactions.filter { it.timestamp.toInt() > (now - 2629743) }.size
        val monthly : MutableList<Pair<String,Int>> = mutableListOf()
        val lastMonthTimestamp = 0
        MONTHLY_TIMESTAMP.values().forEach { month ->
            if(month.timestamp < now){
            val monthlyTransaction = transactions.filter { (it.timestamp.toInt() > lastMonthTimestamp) and (it.timestamp.toInt() < month.timestamp) }.size
            LOGGER.warn("added monthly $monthlyTransaction for $month")
            monthly.add(Pair(month.name,monthlyTransaction))}
        }
        LOGGER.warn("monthly size $monthly.size")
        return TransactionInfo(monthly,transactions.size, lastMonthCount, lastWeekCount, lastDayCount, transactionRepository.getLastUpdate())
    }


    fun addTransactions(transactions: MutableList<Transaction>) {
        transactionRepository.batchInsert(transactions.map { TransactionDTO(it.id, it.timestamp) }, 10000)
        transactionRepository.setLastUpdated()
    }

    fun fetchTransactions() {
        LOGGER.info("Fetch call")
        val client = GraphQLWebClient(
            url = theGraphUrl
        )
        //hard limit for fetching
        val first = 1000
        var latestTimestamp = getLatestTransactionTimestamp()
        var continueLoop = false
        do {
            runBlocking {
                val result = client.execute(AllTransactions(AllTransactions.Variables(first, latestTimestamp)))
                result.data?.let {
                    if (it.transactions.isNotEmpty()) {
                        latestTimestamp = it.transactions.last().timestamp.toInt()
                        addTransactions(it.transactions as MutableList<Transaction>)
                        continueLoop = it.transactions.size == first
                    }
                }
            }
        } while (continueLoop)
    }

    fun getLatestTransactionTimestamp(): Int {
        return transactionRepository.getLatestTransactionTimestamp() ?: 0
    }

    fun getLatestTransaction(): TransactionDTO? {
        return transactionRepository.getLatestTransaction()
    }
}
package com.arians.auctusbackend.service

import com.arians.auctusbackend.generated.AllBurns
import com.arians.auctusbackend.generated.allburns.Burn
import com.arians.auctusbackend.model.BurnDTO
import com.arians.auctusbackend.model.BurnInfo
import com.arians.auctusbackend.model.TimelineNumbers
import com.arians.auctusbackend.repository.BurnRepository
import com.expediagroup.graphql.client.spring.GraphQLWebClient
import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class BurnService(val burnRepository: BurnRepository) {

    companion object {
        private val LOGGER: Logger = LoggerFactory.getLogger(BurnService::class.java)
    }

    @Value("\${theGraph.url}")
    val theGraphUrl: String = ""

    fun fetchBurns() {
        LOGGER.info("Fetch burn call")
        val client = GraphQLWebClient(
            url = theGraphUrl
        )
        //hard limit for fetching
        val first = 1000
        var latestTimestamp = burnRepository.getLatestBurnTimestamp()
        var continueLoop = false
        do {
            runBlocking {
                val result = client.execute(AllBurns(AllBurns.Variables(first, latestTimestamp)))
                result.data?.let {
                    if (it.burns.isNotEmpty()) {
                        latestTimestamp = it.burns.last().tx.timestamp.toInt()
                        addBurns(it.burns as MutableList<Burn>)
                        continueLoop = it.burns.size == first
                    }
                }
            }
        } while (continueLoop)
    }

    fun addBurns(burns: MutableList<Burn>) {
        burnRepository.batchInsert(burns.map {
            BurnDTO(
                id = it.id,
                timestamp = it.tx.timestamp,
                acoName = it.aco.name,
                collateralAmount = it.collateralAmount.toBigDecimal(),
                tokenAmount = it.tokenAmount.toBigDecimal()
            )
        }, 10000)
    }

    fun getBurnInfo(): BurnInfo {
        val burnings = burnRepository.getBurnings()
        //server timezone
        val now = Instant.now().epochSecond
        val lastDayCount = burnings.filter { it.timestamp.toInt() > (now - 86400) }
        val lastWeekCount = burnings.filter { it.timestamp.toInt() > (now - 604800) }
        val lastMonthCount = burnings.filter { it.timestamp.toInt() > (now - 2629743) }

        val tokenAmount = setTokenAmountTimelineNumbers(burnings, lastMonthCount, lastWeekCount, lastDayCount)
        val collateralAmount = setCollateralAmountTimelineNumbers(burnings, lastMonthCount, lastWeekCount, lastDayCount)
        val burningsNumber = TimelineNumbers(burnings.size.toDouble(),lastMonthCount.size.toDouble(),lastWeekCount.size.toDouble(),lastDayCount.size.toDouble())
        return BurnInfo(burnings = burningsNumber, tokenAmount = tokenAmount, collateralAmount = collateralAmount, lastUpdated = burnRepository.getLatestBurnTimestamp().toString())
    }

    private fun setCollateralAmountTimelineNumbers(
        burnings: List<BurnDTO>,
        lastMonthCount: List<BurnDTO>,
        lastWeekCount: List<BurnDTO>,
        lastDayCount: List<BurnDTO>
    ) = TimelineNumbers(overallCount = burnings.sumOf { burn -> burn.collateralAmount.toDouble() },
        lastMonthCount = lastMonthCount.sumOf { burn -> burn.collateralAmount.toDouble() },
        lastWeekCount = lastWeekCount.sumOf { burn -> burn.collateralAmount.toDouble() },
        lastDayCount = lastDayCount.sumOf { burn -> burn.collateralAmount.toDouble() })

    private fun setTokenAmountTimelineNumbers(
        burnings: List<BurnDTO>,
        lastMonthCount: List<BurnDTO>,
        lastWeekCount: List<BurnDTO>,
        lastDayCount: List<BurnDTO>
    ) = TimelineNumbers(overallCount = burnings.sumOf { burn -> burn.tokenAmount.toDouble() },
        lastMonthCount = lastMonthCount.sumOf { burn -> burn.tokenAmount.toDouble() },
        lastWeekCount = lastWeekCount.sumOf { burn -> burn.tokenAmount.toDouble() },
        lastDayCount = lastDayCount.sumOf { burn -> burn.tokenAmount.toDouble() })
}
package com.arians.auctusbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AuctusBackendApplication

fun main(args: Array<String>) {
    runApplication<AuctusBackendApplication>(*args)
}

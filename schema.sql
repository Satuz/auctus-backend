CREATE TABLE transaction (
                         id   VARCHAR(128) NOT NULL,
                         timestamp VARCHAR(30) NOT NULL,
                         PRIMARY KEY (id)
);
# Configure GCP project
provider "google" {
  project = "vaulted-acolyte-312816"
}

# Deploy image to Cloud Run
resource "google_cloud_run_service" "auctus-dashboard" {
  name     = "auctus-backend"
  location = "europe-west4"
 # max_instances = 2
 # service_account_email = "backend-user@vaulted-acolyte-312816.iam.gserviceaccount.com"
 # memory= 1524
 # cpus = 1
 # allow_public_access = false
  template {
    spec {
      containers {
        image = "gcr.io/vaulted-acolyte-312816/auctus-backend"
      }
      service_account_name = "backend-user@vaulted-acolyte-312816.iam.gserviceaccount.com"

    }
  }
  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_sql_database_instance" "master" {
  name             = "db"
  database_version = "POSTGRES_13"
  region           = "europe-west4"

  settings {
    # Second-generation instance tiers are based on the machine
    # type. See argument reference below.
    tier = "db-g1-small"
  }
}

# Create access for dashboard service account
data "google_iam_policy" "auth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "serviceAccount:dashboard-user@vaulted-acolyte-312816.iam.gserviceaccount.com ",
    ]
  }
}
# auctus-backend
Der Service nutzt die GraphQL Schnittstelle von "theGraph", um Daten der Blockchain "auctus" abzurufen.
Er wird in der GCP bereitgestellt als Cloud Run Service.


## the graph
https://thegraph.com/

## auctus
https://auctus.org/

## landscape view
![landscape](../master/landscape.png)
